/*
	CRUD
*/

//CREATE

//insertOne()
db.users.insertOne(
{
	"firstName" : "Tony",
	"lastName" : "Stark",
	"userName" : "iAmIronMan",
	"email" : "iloveyou3000@mail.com",
	"password" : "starkIndustries",
	"isAdmin" : true
}
);


//insertMany()
db.users.insertMany([
	{
		"firstName" : "Pepper",
		"lastName" : "Potts",
	 	"userName" : "rescueArmor",
		"email" : "pepper3000@mail.com",
		"password" : "whereIsTonyAgain",
		"isAdmin" : false
	},
	{
		"firstName" : "Steve",
		"lastName" : "Rogers",
	 	"userName" : "theCaptain",
		"email" : "captAmerica@mail.com",
		"password" : "iCanLiftMjolnirToo",
		"isAdmin" : false
	},
	{
		"firstName" : "Thor",
		"lastName" : "Odinson",
	 	"userName" : "mightyThor",
		"email" : "ThorNotLoki@mail.com",
		"password" : "iAmWorthyToo",
		"isAdmin" : false
	},
	{
		"firstName" : "Loki",
		"lastName" : "Odinson",
	 	"userName" : "godOfMischief",
		"email" : "loki@mail.com",
		"password" : "iAmReallyLoki",
		"isAdmin" : false
	}

])

/*
	Mini-Activity
		- new collection with the name of courses
		- insert the ff:

*/
db.courses.insertMany([
{
	"name" : "Javascript",
	"price" : 3500,
	"description" : "Learn Javascript in a week!",
	"isActive" : true
},
{
	"name" : "HTML",
	"price" : 1000,
	"description" : "Learn Basic HTML in 3 days!",
	"isActive" : true
},
{
	"name" : "CSS",
	"price" : 2000,
	"description" : "Make your website fancy, learn CSS now!",
	"isActive" : true
}
	])


//READ

//db.collections.find() ALL

db.users.find();


//db.collections.findOne ({}) W/FILTER OR QUERY

db.users.find({"isAdmin" : false});


//db.collections.findOne({})

db.users.find({}); //will return all
db.users.findOne({}); //will return the first

//db.collection.find({,}) 

db.users.find({"lastName" : "Odinson" , "firstName" : "Loki"}); //will return loki odinsons


//UPDATE

//db.collections.updateOne({criteria: value}, {$set: {updated value}})

db.users.updateOne({"lastName" : "Potts"}, {$set: {"lastName" : "Stark"}});
 

//db.collections.updateMany({criteria: value}, {$set : {updated value and fieldset}} )

db.users.updateMany({"lastName" : "Odinson"}, {$set : {"isAdmin" : true}});


//db.collection.update({}, {$set : {updatedField:updated value}})

db.users.updateOne({} , {$set : {"email" : "starkindustries@mail.com"}});


/*
	Mini-Activity
		- update JavaScript course 
			- make isActive : false

*/

db.courses.updateOne({"name" : "Javascript"}, {$set : {"isActive" : false}});

db.courses.updateMany({} , {$set : {"enrollees" : 10}}) //will add another new fieldset : value



//DELETE

//db.collections.deleteOne({criteria : value})

db.users.deleteOne({"isAdmin" : false});


//db.collections.deleteMany({criteria : value})

db.users.deleteMany({"lastName" : "Odinson"});


// db.collections.deleteOne({});

db.users.deleteOne({});


//db.collections.deleteMany({});

db.users.deleteMany({});

